<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Hash;
use App\PenjualKonven;
use App\Pembeli;
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller
{
  public function __construct()
  {
      //
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    // START OF API CONTROLLER
   public function api_loginPelayan(Request $request){
      if(Auth::attempt([
          'email' => request('email'),
          'password' => request('password'),
          'level' => 'Pelayan'
      ])){
          $user = Auth::user();
          $email = $request->get('email');
          $password = $request->get('password');
          $success['token'] =  $user->createToken('MyApp')-> accessToken;
          $success['email'] = $email;
          $success['password'] = $password;

          $pelayan = User::where('email', $success['email'])->first();
          $pelayan->token = $success['token'];
          // dd($pembeli);
          return response()->json([
              // 'error'=>false,
              'status'=>'success',
              // 'token' => $success['token'],
              'user' => $pelayan
          ]);
      }
      else{
          $success['status'] = 'failed';
          $success['error'] = 'Unauthorised';
          $success['message'] = 'Your email or password incorrect!';
          return response()->json([$success],401);
      }
  }

  public function api_loginKasir(Request $request){
     if(Auth::attempt([
         'email' => request('email'),
         'password' => request('password'),
         'level' => 'Kasir'
     ])){
         $user = Auth::user();
         $email = $request->get('email');
         $password = $request->get('password');
         $success['token'] =  $user->createToken('MyApp')-> accessToken;
         $success['email'] = $email;
         $success['password'] = $password;

         $kasir = User::where('email', $success['email'])->first();
         $kasir->token = $success['token'];
         // dd($pembeli);
         return response()->json([
            'error'=>false,
             'status'=>'success',
             // 'token' => $success['token'],
             'user' => $kasir
         ]);
     }
     else{
         $success['status'] = 'failed';
         $success['error'] = 'Unauthorised';
         $success['message'] = 'Your email or password incorrect!';
         return response()->json([$success],401);
     }
 }

 public function api_logout(Request $request)
 {
   dd($request->user());
   $request->user()->token()->revoke();
   return response()->json([
     'message' => 'Successfully logged out'
   ]);
 }  
  
  public function api_produk()
  {
    $produk = 'App\BarangKonven'::get();
    return response()->json(['status'=>'success','menu'=>$produk]);
  }
  
  public function gambar($id)
  {
      
      $user = User::find($id);
      $userFoto = $user->foto;
      
       //dd($gambarURL);
    $img = 'Illuminate\Support\Facades\Storage'::get('/images/'.$userFoto);
    
    return response($img)->header('Content-type','image/png');
  
  }
}
