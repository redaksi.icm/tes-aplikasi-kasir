@extends('layout')

@section('css')
<link href="{{asset('lib/highlightjs/github.css')}}" rel="stylesheet">
<link href="{{asset('lib/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('breadcrumbs')
<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{url('/')}}">Home</a>
    <a class="breadcrumb-item" href="#">Data Master</a>
    <a class="breadcrumb-item" href="{{url('master/status')}}">Status</a>
</nav>
@stop

@section('title')
<div class="sl-page-title">
    <h5>Status</h5>
</div><!-- sl-page-title -->
@stop

@section('content')
	<div class="card pd-10 pd-sm-20">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <a href="{{url('master/status/add')}}" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus mg-r-10"></i> Tambah Status</a>
        </div><!-- col-sm -->
    </div>

    <div class="table-wrapper mg-t-10">
        <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th>No</th>
                <!-- <th>Id</th> -->
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($list as $value => $stat)
              <tr>
                  <td align="center">{{$value +1}}</td>
                  <!-- <td align="center">{{$stat->id}}</td> -->
                  <td class="col-sm-7">{{$stat->status}}</td>
                  <td>
                      <a href="{{url('master/status/edit/'.$stat->id.'')}}" class="btn btn-warning btn-icon rounded-circle mg-r-5 mg-b-10" title="Edit">
                          <div><i class="fa fa-pencil"></i></div>
                      </a>
                      <a href="{{url('master/status/delete/'.$stat->id.'')}}" class="btn btn-danger btn-icon rounded-circle mg-r-5 mg-b-10" title="Delete">
                          <div><i class="fa fa-trash"></i></div>
                      </a>
                  </td>
              </tr>
              @endforeach
            </tbody>
        </table>
      </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

@endsection
