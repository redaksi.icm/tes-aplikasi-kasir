@extends('template.main')
@section('content')
          <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{$title}}</h3>
        </div>
        <div class="box-body">
          <p>Selamat Datang <strong>{{auth()->user()->level}}</strong>!!</p>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

@endsection

