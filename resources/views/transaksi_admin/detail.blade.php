@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="{{url('/transaksi')}}">Transaksi</a> /
  <a href="{{url('#')}}">Detail</a>
@stop

@section('title')
  <h3>Transaksi</h3>
@stop

@section('content')
<a class="btn btn-success btn-sm" href="{{url('invoice_admin/'.$transaksi.'')}}"><i class="fa fa-file"></i> Print Nota</a><br>
            <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2>Detail Transaksi<small></small></h2>
                        <div class="clearfix">
                        </div>
                      </div>
                      
                      <div class="x_content">
                        {{-- <a href="{{url('master/bank/add')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus mg-r-10"></i> Add Bank</a> --}}
                        <table id="datatable1" class="table table-hover">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Barang</th>
                              <th>Id Transaksi</th>
                              <th>Jumlah</th>
                              <th>Harga</th>
                              <th>Diskon</th>
                              <th>Total</th>
                              <th>Tanggal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $value => $transaksi)
                            <tr>
                              <td align="center">{{$value+1}}</td>
                              <td>{{$transaksi->nama}}</td>
                              <td>{{$transaksi->transaksi_konven_id}}</td>
                              <td>{{$transaksi->jumlah}}</td>
                              <td>{{$transaksi->harga}}</td>
                              <td>{{$transaksi->diskon}}</td>
                              <td>{{$transaksi->total}}</td>
                              <td>{{$transaksi->created_at}}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <div class="form-group">
                          <div>
                            <a class="btn btn-primary" onclick="location.href='{{URL::previous()}}'">Back</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
@endsection

@section('javascript')

    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>

@endsection
