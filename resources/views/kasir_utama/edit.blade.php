@section('js')


@stop
@extends('layout_pos')

@section('content')
{!! Form::open(array('url'=>'transaksi_konven_kasir/update_transaksi_konven/'.$data->id.'', 'method'=>'POST','files'=>'true'))!!}
  <section class="content-header">
    <h1>
      Edit Transaksi
      <small>Kantin Kami</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-pencil"></i> Transaksi</a></li>
      <li class="active"> Edit Transaksi</li>
    </ol>
  </section>

  <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>


  <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Transaksi</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <div class="form-group">
                <div class="col-md-8">
                <label for="nama">No Meja<span class="required" style="color:red">*</span></label>
                <input class="form-control" placeholder="No Meja" name="no_meja" value="{{(isset($data->no_meja))?$data->no_meja:''}} " type="text">
                  @if ($errors->has('no_meja'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('no_meja') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-8">
                <label for="deskripsi">Total Diskon</label>
                  <input class="form-control" cols="1" rows="2" placeholder="Diskon" name="diskon" value="{{(isset($data->diskon))?$data->diskon:''}} " type="text">
                  @if ($errors->has('diskon'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('diskon') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-8">
                <label for="harga">Total Belanja<span class="required" style="color:red">*</span></label>
                <input class="form-control" placeholder="Total" name="total" value="{{(isset($data->total))?$data->total:''}} " type="text">
                  @if ($errors->has('total'))
                  <span class="invalid-feedback" role="alert" style="color: red">
                      <strong>{{ $errors->first('total') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
            
            <div class="form-group">
                <div class="col-md-8"><br>
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" onclick="location.href='{{url('transaksi_konven_kasir/find')}}'">Batal</button>
                </div>
            </div>
          </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section>
    
{!!Form::close()!!}

        <!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').dataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(1000);</script>

@endsection
