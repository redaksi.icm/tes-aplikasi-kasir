@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">Laporan</a> /
  <a href="{{url('laporan/pembeli')}}">Pembeli</a>

@stop

@section('title')
  <h3>Laporan</h3>
@stop

@section('content')
<div class="col-md-1">
  {!! Form::open(array('url'=>'laporan/pembeli/downloadExcel', 'method'=>'GET', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!}
  <input type="hidden" name="pembeli" value="{{$requested->pembeli}}">
  <input type="hidden" name="from" value="{{$requested->from}}">
  <input type="hidden" name="until" value="{{$requested->until}}">
  <button class="fa fa-download btn btn-success" type="submit"> Excel </button>
  {!!Form::close()!!}
</div>
<div class="col-md-1">
  {!! Form::open(array('url'=>'laporan/pembeli/downloadPDF', 'method'=>'GET', 'class'=>'form-horizontal form-label-left', 'novalidate'))!!}
  <input type="hidden" name="pembeli" value="{{$requested->pembeli}}">
  <input type="hidden" name="from" value="{{$requested->from}}">
  <input type="hidden" name="until" value="{{$requested->until}}">
  <button class="fa fa-download btn btn-success" type="submit"> PDF </button>
  {!!Form::close()!!}
</div>
  @if (count($errors)>0)
    <div id="myError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                    <h4 style="color:red">Ooops...</h4>
                    @foreach($errors->all() as $error)
                      <a>{{$error}}<br></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
  @endif
  <div class="x_panel">
    <div class="x_title">
      <h2 style="margin-top:15px">Laporan Pembeli</h2>
      {!! Form::open(array('url'=>'laporan/pembeli/find', 'method'=>'GET', 'class'=>'form-horizontal form-label-left pull-right', 'novalidate'))!!}
            <div class="item form-group pull-right">
              <button class="btn btn-primary pull-right" type="submit"> Cari! </button>
              <div class="col-md-3 col-sm-4 col-xs-12 pull-right">
                <input type="text" name="until" class="form-control datepicker" value="{{\Carbon\Carbon::parse($requested->until)->addDay('-1')->format('m/d/Y')}}">
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12 pull-right">
                {{-- <input type="hidden" name="pembeli" value="{{$requested->pembeli}}"> --}}
                <input type="text" name="from" class="form-control datepicker" value="{{\Carbon\Carbon::parse($requested->from)->format('m/d/Y')}}">
              </div>
              <div class="col-md-3 col-sm-4 col-xs-12 pull-right">
                <select class="form-control select2" name="pembeli" class="form-control col-md-7 col-xs-12">
                    @foreach($pembeliAll as $value => $key)
                        <option value="{{$key->id}}" @if($key->id == $pembeli->id) selected @endif>{{$key->name}}</option>
                    @endforeach
                </select>
              </div>
            </div>
      {!!Form::close()!!}
      <div class="clearfix"></div>
    </div>


    
    <div class="x_content">
      <div class="col-md-12">
        <h3>{{$pembeli->name}}</h3>
        <ul class="list-unstyled user_data">
          <li><i class="fa fa-money user-profile-icon">Total Belanja</i> Rp {{number_format($outcomeTotal,0,".",".")}},-
          </li>
          <li><i class="fa fa-shopping-cart user-profile-icon">Total Produk Yang dibeli</i> {{$itemTotal}} Produk
          </li>
        </ul>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <table id="datatable1" class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Produk</th>
              <th>Toko</th>
              <th>Harga</th>
              <th>Diskon</th>
              <th>Jumlah</th>
              <th>Total</th>
              <th>Jenis Pembayaran</th>
              <th>Status</th>
            </tr>
          </thead>
          @if (\Carbon\Carbon::parse($requested->from)->addDay(1) == \Carbon\Carbon::parse($requested->until))
            <tbody>
              @foreach($list as $value => $detail_transaksi)
                <tr>
                    <td align="center">{{$value+1}}</td>
                    <td>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->format('H:i:s')}}</td>
                    <td>{{$detail_transaksi->nama}}</td>
                    <td>{{$detail_transaksi->nama_toko}}</td>
                    <td>Rp {{number_format($detail_transaksi->harga,0,".",".")}},-</td>
                    <td>Rp {{number_format($detail_transaksi->diskon,0,".",".")}},-</td>
                    <td>{{$detail_transaksi->jumlah}}</td>
                    <td>Rp {{number_format($detail_transaksi->total,0,".",".")}},-</td>
                    <td>{{$detail_transaksi->jenis_pembayaran}}</td>
                    <td>{{$detail_transaksi->status}}</td>
                </tr>
              @endforeach
            </tbody>
          @else
            <tbody>
              @foreach($list as $value => $detail_transaksi)
                <tr>
                    <td align="center">{{$value+1}}</td>
                    <td>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}<br>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->format('H:i:s')}}</td>
                    <td>{{$detail_transaksi->nama}}</td>
                    <td>{{$detail_transaksi->nama_toko}}</td>
                    <td>Rp {{number_format($detail_transaksi->harga,0,".",".")}},-</td>
                    <td>Rp {{number_format($detail_transaksi->diskon,0,".",".")}},-</td>
                    <td>{{$detail_transaksi->jumlah}}</td>
                    <td>Rp {{number_format($detail_transaksi->total,0,".",".")}},-</td>
                    <td>{{$detail_transaksi->jenis_pembayaran}}</td>
                    <td>{{$detail_transaksi->status}}</td>
                </tr>
              @endforeach
            </tbody>
          @endif


        </table>
        <a class="btn btn-primary" onclick="location.href='{{url('laporan/pembeli')}}'">Back</a>
      </div>
    </div>




  </div>
  @if (session('error'))
    <div id="myError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                    <h4 style="color:red">Ooops...</h4>
                      <a>{{ session('error') }}</a>
                </div>
            </div>
        </div>
    </div>
  @endif
@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
      <script>
          $(function(){
              'use strict';

              $('#datatable1').DataTable({
  //                scrollX: true,
                  responsive: false,
                  language: {
                      searchPlaceholder: 'Search...',
                      sSearch: '',
                      lengthMenu: '_MENU_ items/page',
                  }
              });
              // $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
              $('.select2').select2();
          });
      </script>
      <script type="text/javascript">
       $(function(){
        $(".datepicker").datepicker({
          // format: "dd/mm/yyyy",
          endDate: "dd",
          autoclose: true,
          todayHighlight: true,
        });
       });
      </script>
      <script>$("#myModalError").modal("show");</script>
      <script>$("#myError").modal("show");</script>



@endsection
