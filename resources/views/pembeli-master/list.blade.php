@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    {{-- <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet"> --}}

@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashborad</a> /
  <a href="#">User</a> /
  <a href="{{url('/master/pembeli')}}">Pembeli</a>
@stop

@section('title')
  <h3>Pembeli</h3>
@stop


@section('content')
  <div id="app">
    @if(Session::has('success'))
      <div class="alert alert-success">
        {{ Session::get('success') }}
      </div>
    @elseif(Session::has('error'))
      <div class="alert alert-error">
        {{ Session::get('error') }}
      </div>
    @endif
  </div>

  <div class="exportexcel">    
  <form action="{{ url('master/pembeli/export') }}" method="get" class="form-horizontal pull-left" enctype="multipart/form-data">
    <a href="{{url('master/pembeli/add')}}" class="btn btn-primary"><i class="fa fa-plus mg-r-10 fa fa-download"></i>Tambah Pembeli</a>    
      <button type="submit" class="btn btn-success">Export Excel<i class="mg-r-10 fa fa-download"></i></button>
  </form>      
  </div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        @if (Request::url() == url('master/pembeli/aktif'))
          <div class="x_title">
            <h2>Pembeli Aktif<small>dari Data Master</small></h2>
            <div class="input-group-btn">
              <button  style="margin-bottom:10px" type="button" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="{{url('master/pembeli/')}}">Semua Pembeli</a>
                </li>
                <li><a href="{{url('master/pembeli/aktif')}}">Pembeli Aktif</a>
                </li>
                <li><a href="{{url('master/pembeli/non-aktif')}}">Pembeli Tidak Aktif</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @elseif (Request::url() == url('master/pembeli/non-aktif'))
          <div class="x_title">
            <h2>Pembeli Tidak Aktif<small>dari Data Master</small></h2>
            <div class="input-group-btn">
              <button  style="margin-bottom:10px" type="button" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="{{url('master/pembeli/')}}">Semua Pembeli</a>
                </li>
                <li><a href="{{url('master/pembeli/aktif')}}">Pembeli Aktif</a>
                </li>
                <li><a href="{{url('master/pembeli/non-aktif')}}">Pembeli Tidak Aktif</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @else
          <div class="x_title">
            <h2>Semua Pembeli<small>dari Data Master</small></h2>
            {{-- <h2>All Buyer<small>from Master Data</small></h2> --}}
            <div class="input-group-btn">
              <button  style="margin-bottom:10px" type="button" class="btn btn-default btn-sm dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false">Filter <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="{{url('master/pembeli/')}}">Semua Pembeli</a>
                </li>
                <li><a href="{{url('master/pembeli/aktif')}}">Pembeli Aktif</a>
                </li>
                <li><a href="{{url('master/pembeli/non-aktif')}}">Pembeli Tidak Aktif</a>
                </li>
              </ul>
            </div>
            <div class="clearfix">
            </div>
          </div>
        @endif
        <div class="x_content">
          <table id="datatable1" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th class="col-md-1">No</th>
                <th>Id</th>
                <th>Nama Pembeli</th>
                <th>Email</th>
                <th>No HP</th>
                <th class="col-md-1">Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($list as $value => $pembeli)
              <tr class="item-{{$pembeli->id}}">
                  <td align="center">{{$value+1}}</td>
                  <td>{{$pembeli->id}}</td>
                  <td>{{$pembeli->name}}</td>
                  <td>{{$pembeli->email}}</td>
                  <td>{{$pembeli->no_telepon}}</td>
                  <td>
                    @if($pembeli->status_id==1)
                      <a style="width:70px" href="javascript:if(confirm('Ingin mengubah status pembeli?')){window.location.href='{{ url('master/pembeli/status/'.$pembeli->user_id.'')}}'};" onclick="" class="btn btn-primary btn-xs rounded-circle mg-r-5 mg-b-10" title="Aktive">
                        <div><i class="fa fa-toggle-on"></i> Aktif</div>
                      </a>
                    @else
                      <a style="width:70px" href="javascript:if(confirm('Ingin mengubah status pembeli?')){window.location.href='{{ url('master/pembeli/status/'.$pembeli->user_id.'')}}'};" onclick="" class="btn btn-default btn-xs rounded-circle mg-r-5 mg-b-10" title="Inaktive">
                        <div><i class="fa fa-toggle-off"></i> Off</div>
                      </a>
                    @endif
                  </td>
                  <td>
                      <a href="{{url('master/pembeli/'.$pembeli->id.'')}}" class="btn btn-warning btn-icon rounded-circle mg-r-5 mg-b-10" title="Edit">
                          <div><i class="fa fa-pencil"></i></div>
                      </a>
                      <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{ url('master/pembeli/delete/'.$pembeli->id.'')}}'};" onclick="" class="btn btn-danger btn-icon rounded-circle mg-r-5 mg-b-10" title="Delete">
                          <div><i class="fa fa-trash"></i></div>
                      </a>
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div id="deleteModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body text-center">
                  <i class="fa fa-4x fa-trash"></i>
                  <h5>Yakin ingin hapus data?</h5>
                  <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Kembali
                  </button>
                  <a href="{{ url('master/pembeli/delete/'.$pembeli->id.'')}}" onclick="" class="btn btn-danger btn-icon rounded-circle mg-r-5 mg-b-10" title="Delete">Hapus</a>
              </div>
          </div>
      </div>
  </div>


@endsection

@section('javascript')

      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
      {{-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
      <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                // aaSorting: [[3,'asc']],
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
            // $(document).ready(function() {
            //     var t = $('#datatable1').DataTable( {
            //         "columnDefs": [ {
            //             "searchable": true,
            //             "orderable": false,
            //             "targets": 0
            //         } ],
            //         "order": [[ 2, 'asc' ]]
            //     } );
            //
            //     t.on( 'order.dt search.dt', function () {
            //         t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //             cell.innerHTML = i+1;
            //         } );
            //     } ).draw();
            // } );

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">
        var id;
        var id_to_delete;
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#deleteModal').modal('show');
            id_to_delete = $(this).data('id');
            // console.log(id_to_delete);
        });
        $('.modal-body').on('click', '.delete', function() {
          // console.log("click");
            $.ajax({
                type: 'GET',
                headers: {
                  'Accept': 'application/json'
                },
                url: '{{ env('APP_URL') }}/api/master/pembeli/delete/' + id_to_delete,
                success: function(data) {
                  console.log("coba"+data);
                    // toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item-' + id_to_delete).remove();
                    $('.col1').each(function (index) {
                        $(this).html(index+1);
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(3000);</script>

@endsection
