<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>

    <style>
    .text-center{
      text-align: center;
    }
    table, td, th {
      border: 0.7px solid;
      text-align: left;
    }

    table {
      border-collapse: collapse;
      width: 100%;
    }

    th, td {
      padding: 7px;
    }
    .header{
      font-size: 20px;
      font-style: oblique;
      font-weight: bold;
    }
    </style>
  </head>
  <body>
    <label class="header"> <b>GAMAPAY</b> <br> Universitas Gadjah Mada</label>
    <hr>
    <div>
      <label style="padding-right: 58px;"> Toko : {{$penjual->name}} </label>
    </div>
    <div>
      <label style="padding-right: 58px;"> Email : {{$penjual->email}}</label>
    </div>
    <div>
      <label style="padding-right: 0px;"> No.Handphone : {{$penjual->no_telepon}}</label>
    </div>
    <div>
      @if (\Carbon\Carbon::parse($request->from) == \Carbon\Carbon::parse($request->until)->addDay(-1))
      <label style="padding-right: 36px;"> Tanggal : {{\Carbon\Carbon::parse($request->from)->formatLocalized('%A, %d %B %Y')}}</label>
      @else
      <label style="padding-right: 36px;">Tanggal : {{\Carbon\Carbon::parse($request->from)->formatLocalized('%A, %d %B %Y')}} - {{\Carbon\Carbon::parse($request->until)->formatLocalized('%A, %d %B %Y')}}</label>
      @endif
    </div>
    <div>
      <label style="padding-right: 8px;"> Total Pemasukan : {{$incomeTotal}} </label>
    </div>
    <div>
      <label style="padding-right: 32px;"> Produk Terjual  : {{$itemTotal}} </label>
    </div>
    <br>
    <table id="datatable1" class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>No</th>
          <th>Id Transaksi</th>
          <th>Tanggal</th>
          <th>Produk</th>
          <th>Pembeli</th>
          <th>Harga</th>
          <th>Diskon</th>
          <th>Jumlah</th>
          <th>Total</th>
          <th>Jenis Pembayaran</th>
          <th>Status</th>
        </tr>
      </thead>
      @if (\Carbon\Carbon::parse($request->from)->addDay(1) == \Carbon\Carbon::parse($request->until))
        <tbody>
          @foreach($list as $value => $detail_transaksi)
            <tr>
                <td align="center">{{$value+1}}</td>
                <td>{{$detail_transaksi->transaksi_konven_id}}</td>
                <td>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->format('H:i:s')}}</td>
                <td>{{$detail_transaksi->nama_barang}}</td>
                <td>{{$detail_transaksi->nama_pembeli}}</td>
                <td>{{$detail_transaksi->harga}}</td>
                <td>{{$detail_transaksi->diskon}}</td>
                <td>{{$detail_transaksi->jumlah}}</td>
                <td>{{$detail_transaksi->total}}</td>
                <td>{{$detail_transaksi->jenis_pembayaran}}</td>
                <td>{{$detail_transaksi->status}}</td>
            </tr>
          @endforeach
        </tbody>
      @else
        <tbody>
          @foreach($list as $value => $detail_transaksi)
            <tr>
                <td align="center">{{$value+1}}</td>
                <td>{{$detail_transaksi->transaksi_konven_id}}</td>
                <td>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}<br>{{\Carbon\Carbon::parse($detail_transaksi->updated_at)->format('H:i:s')}}</td>
                <td>{{$detail_transaksi->nama_barang}}</td>
                <td>{{$detail_transaksi->nama_pembeli}}</td>
                <td>{{$detail_transaksi->harga}}</td>
                <td>{{$detail_transaksi->diskon}}</td>
                <td>{{$detail_transaksi->jumlah}}</td>
                <td>{{$detail_transaksi->total}}</td>
                <td>{{$detail_transaksi->jenis_pembayaran}}</td>
                <td>{{$detail_transaksi->status}}</td>
            </tr>
          @endforeach
        </tbody>
      @endif


    </table>
    <label> <b>Aplikasi Kasir Berbasis Web Gamapay.</b></label>
  </body>
</html>
