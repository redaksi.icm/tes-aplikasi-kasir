(function( $ ) {
	"use strict";

	$(function() {
		$( window ).konami({
			cheat: function() {
				alert(
					'Alih Daya\n' +
					'version 0.1\n\n' +
					'Created by:\n' +
					'MUHAMMAD FAHMI RIZAL [8511250Z]' +
					'\n\nThis program is dedicated to my father:\n' +
					'MASHUDI [5374431K3]');
			} // end cheat
		});
	});
}(jQuery));